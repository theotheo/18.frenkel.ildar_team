#!/usr/bin/python2

#---------------------------

from bisect import bisect_left, bisect_right

class SortedCollection(object):
    '''Sequence sorted by a key function.

    SortedCollection() is much easier to work with than using bisect() directly.
    It supports key functions like those use in sorted(), min(), and max().
    The result of the key function call is saved so that keys can be searched
    efficiently.

    Instead of returning an insertion-point which can be hard to interpret, the
    five find-methods return a specific item in the sequence. They can scan for
    exact matches, the last item less-than-or-equal to a key, or the first item
    greater-than-or-equal to a key.

    Once found, an item's ordinal position can be located with the index() method.
    New items can be added with the insert() and insert_right() methods.
    Old items can be deleted with the remove() method.

    The usual sequence methods are provided to support indexing, slicing,
    length lookup, clearing, copying, forward and reverse iteration, contains
    checking, item counts, item removal, and a nice looking repr.

    Finding and indexing are O(log n) operations while iteration and insertion
    are O(n).  The initial sort is O(n log n).

    The key function is stored in the 'key' attibute for easy introspection or
    so that you can assign a new key function (triggering an automatic re-sort).

    In short, the class was designed to handle all of the common use cases for
    bisect but with a simpler API and support for key functions.

    >>> from pprint import pprint
    >>> from operator import itemgetter

    >>> s = SortedCollection(key=itemgetter(2))
    >>> for record in [
    ...         ('roger', 'young', 30),
    ...         ('angela', 'jones', 28),
    ...         ('bill', 'smith', 22),
    ...         ('david', 'thomas', 32)]:
    ...     s.insert(record)

    >>> pprint(list(s))         # show records sorted by age
    [('bill', 'smith', 22),
     ('angela', 'jones', 28),
     ('roger', 'young', 30),
     ('david', 'thomas', 32)]

    >>> s.find_le(29)           # find oldest person aged 29 or younger
    ('angela', 'jones', 28)
    >>> s.find_lt(28)           # find oldest person under 28
    ('bill', 'smith', 22)
    >>> s.find_gt(28)           # find youngest person over 28
    ('roger', 'young', 30)

    >>> r = s.find_ge(32)       # find youngest person aged 32 or older
    >>> s.index(r)              # get the index of their record
    3
    >>> s[3]                    # fetch the record at that index
    ('david', 'thomas', 32)

    >>> s.key = itemgetter(0)   # now sort by first name
    >>> pprint(list(s))
    [('angela', 'jones', 28),
     ('bill', 'smith', 22),
     ('david', 'thomas', 32),
     ('roger', 'young', 30)]

    '''

    def __init__(self, iterable=(), key=None):
        self._given_key = key
        key = (lambda x: x) if key is None else key
        decorated = sorted((key(item), item) for item in iterable)
        self._keys = [k for k, item in decorated]
        self._items = [item for k, item in decorated]
        self._key = key

    def _getkey(self):
        return self._key

    def _setkey(self, key):
        if key is not self._key:
            self.__init__(self._items, key=key)

    def _delkey(self):
        self._setkey(None)

    key = property(_getkey, _setkey, _delkey, 'key function')

    def clear(self):
        self.__init__([], self._key)

    def copy(self):
        return self.__class__(self, self._key)

    def __len__(self):
        return len(self._items)

    def __getitem__(self, i):
        return self._items[i]

    def __iter__(self):
        return iter(self._items)

    def __reversed__(self):
        return reversed(self._items)

    def __repr__(self):
        return '%s(%r, key=%s)' % (
            self.__class__.__name__,
            self._items,
            getattr(self._given_key, '__name__', repr(self._given_key))
        )

    def __reduce__(self):
        return self.__class__, (self._items, self._given_key)

    def __contains__(self, item):
        k = self._key(item)
        i = bisect_left(self._keys, k)
        j = bisect_right(self._keys, k)
        return item in self._items[i:j]

    def index(self, item):
        'Find the position of an item.  Raise ValueError if not found.'
        k = self._key(item)
        i = bisect_left(self._keys, k)
        j = bisect_right(self._keys, k)
        return self._items[i:j].index(item) + i

    def count(self, item):
        'Return number of occurrences of item'
        k = self._key(item)
        i = bisect_left(self._keys, k)
        j = bisect_right(self._keys, k)
        return self._items[i:j].count(item)

    def insert(self, item):
        'Insert a new item.  If equal keys are found, add to the left'
        k = self._key(item)
        i = bisect_left(self._keys, k)
        self._keys.insert(i, k)
        self._items.insert(i, item)

    def insert_right(self, item):
        'Insert a new item.  If equal keys are found, add to the right'
        k = self._key(item)
        i = bisect_right(self._keys, k)
        self._keys.insert(i, k)
        self._items.insert(i, item)

    def remove(self, item):
        'Remove first occurence of item.  Raise ValueError if not found'
        i = self.index(item)
        del self._keys[i]
        del self._items[i]

    def find(self, k):
        'Return first item with a key == k.  Raise ValueError if not found.'
        i = bisect_left(self._keys, k)
        if i != len(self) and self._keys[i] == k:
            return self._items[i]
        raise ValueError('No item found with key equal to: %r' % (k,))

    def find_le(self, k):
        'Return last item with a key <= k.  Raise ValueError if not found.'
        i = bisect_right(self._keys, k)
        if i:
            return self._items[i-1]
        raise ValueError('No item found with key at or below: %r' % (k,))

    def find_lt(self, k):
        'Return last item with a key < k.  Raise ValueError if not found.'
        i = bisect_left(self._keys, k)
        if i:
            return self._items[i-1]
        raise ValueError('No item found with key below: %r' % (k,))

    def find_ge(self, k):
        'Return first item with a key >= equal to k.  Raise ValueError if not found'
        i = bisect_left(self._keys, k)
        if i != len(self):
            return self._items[i]
        raise ValueError('No item found with key at or above: %r' % (k,))

    def find_gt(self, k):
        'Return first item with a key > k.  Raise ValueError if not found'
        i = bisect_right(self._keys, k)
        if i != len(self):
            return self._items[i]
        raise ValueError('No item found with key above: %r' % (k,))

#----------------------------

from xml.etree.ElementTree import iterparse
import sys, getopt, os

def main(argv):

	# !!! Attention please - shitcode here !!!	
	
	matrfields = []
	'''
	matrfields = {	'ANKHD1': 0,     # for 4 samp
					'PCDH1': 1,
					'CCDC85C': 2,
					'SETD3': 3,
					'CSE1L': 4,
					'ENSG00000236127': 5,
					'CYTH1': 6,
					'EIF3H': 7,
					'DHX35': 8,
					'ITCH': 9,
					'NFS1': 10,
					'PREX1': 11,
					'RARA': 12,
					'PKIA': 13,
					'SUMF1': 14,
					'LRRFIP2': 15,
					'TATDN1': 16,
					'GSDMB': 17,
					'WDR67': 18,
					'ZNF704': 19 }

	matrfields = { 'ACACA': 0,  # for 3 samp
					'STAC2': 1, 
					'CPNE1': 2, 
					'PI3': 3, 
					'DIDO1': 4,
					'KIAA0406': 5,
					'GLB1': 6,
					'CMTM7': 7,
					'LAMP1': 8,
					'MCF2L': 9,
					'RAB22A': 10,
					'MYO9B': 11,
					'RPS6KB1': 12,
					'SNF8': 13,
					'SKA2': 14,
					'MYO19': 15,
					'STARD3': 16,
					'DOK5': 17,
					'VAPB': 18,
					'IKZF3': 19,
					'ZMYND8': 20,
					'CEP250': 21 }
	'''
	lalmatr = [[0]]
	'''
	for i in xrange(len(matrfields.keys())):
		#print i
		lalmatr.append([])
		for j in xrange(len(matrfields.keys())):
			lalmatr[i].append(0)
	'''

	
	MAXDISTANCE = 5
	LEASTIDENTITY = 95.
	MINTOTALALIGN = 30
	outfmt = "%80s %12s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s\n"
	exechelp = '\n\
	Basic Usage: findpairs.py -i <inputfile> \n\
\n\
	Findpairs\n\
\n\
	File options:\n\
				-i, --ifile    =FILENAME  input file name \n\
                                          (stdin if not specified)\n\
\n\
    Matching options:\n\
				-d, --distance =INT       max gap between two aligned chimera read parts\n\
				    --identcull=FLOAT     minimum accepted identity of aligned read\n\
					--sumalign =INT       minimum accepted total aligned\n\
'

	inputfile = sys.stdin

	try:
		opts, args = getopt.getopt(argv,"hi:o:d:",["ifile=","ofile=","distance=","identcull="])
	except getopt.GetoptError:
		print exechelp
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print exechelp
			sys.exit()
		elif opt in ("-i", "--ifile"):
			inputfile = arg
		elif opt in ("-d", "--distance"):
			try:
				MAXDISTANCE = int(arg)
			except:
				print "Invalid distance parameter"
				sys.exit(-1)
		elif opt in ("--identcull"):
			try:
				LEASTIDENTITY = float(arg)
			except:
				print "Invalid least identity parameter"
				sys.exit(-1)
		elif opt in ("--sumalign"):
			try:
				MINTOTALALIGN = int(arg)
			except:
				print "Invalid minimal accepted total align length"
				sys.exit(-1)
	
	try:
		if inputfile!=sys.stdin:
			with open(inputfile,'r') as f:
				datarows=[x.strip().split('\t') for x in f]
		else:
			with sys.stdin as f:
				datarows=[x.strip().split('\t') for x in f]
		#print datarows

	except:
		print "Failed to open input file!"
		sys.exit(2)
	
	import subprocess
	p = subprocess.Popen("date --rfc-3339=\'seconds\'", stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
	if inputfile!=sys.stdin:
		filename = 'Res_' + os.path.basename(inputfile) + "_reads_" + output[:19] + ".txt"
	else:
		filename = 'Res_' + "<stdin>" + "_reads_" + output[:19] + ".txt"
	try:
		f = open(filename, 'w')
	except:
		print "Failed to open output file!"
		sys.exit(2)
	
	f.write(outfmt % (	'READ-ID', 
						'READ-LEN', 
						'GENE1 ID', 
						'G1-HIT-FROM', 
						'G1-HIT-TO', 
						'G1-QUERY-FROM', 
						'G1-QUERY-TO', 
						'GENE2 ID', 
						'G2-HIT-FROM', 
						'G2-HIT-TO', 
						'G2-QUERY-FROM', 
						'G2-QUERY-TO', 
						'QUERY-LENDIFF')	)
	f.write('-'*325 + '\n')
	
	readctr = 0
	matchctr = 0
	
	matchdb = []
	prevprocessed = ''
	for i_row in xrange(len(datarows)):
	
		# all single read filters go here
		
		if float(datarows[i_row][2]) < LEASTIDENTITY : #  identity > xx %
		#if i_hsp.find('Hsp_identity').text != i_hsp.find('Hsp_align-len').text:
			continue
			
		
		# processing
		if (prevprocessed != '') and (datarows[i_row][0] != prevprocessed):
			
			headmatchdb = SortedCollection(matchdb, key=lambda a: a["query_from"])
	
			#tailmatchdb = SortedCollection(matchdb, key=lambda a: a["query_to"])
	
			#print headmatchdb
			#print tailmatchdb

			#leftscope = 0
	
			# TODO can be optimized with second from-sorted array and left side scope culling, but is ok as it is.
			for i in headmatchdb:
				try:
					found = headmatchdb.find_gt(i['query_to'])
				except:
					#print "No match for " + str(i['query_to'])
					continue
	
				#if ((found['query_from'] - i['query_to']) <= MAXDISTANCE) and (i['hit_id'] != found['hit_id']):
				if ((found['query_from'] - i['query_to']) <= MAXDISTANCE) and (datarows[i['hitindex']][1] != datarows[found['hitindex']][1]):
			
					# FILTER!
					if (int(datarows[i["hitindex"]][3]) + int(datarows[found["hitindex"]][3])) < MINTOTALALIGN:
						continue
					
					#print str(found['query_from']) + "... matches ..." + str(i['query_to']) + " with diff " + str(found['query_from'] - i['query_to'])
					matchctr = matchctr + 1
					
					try:
						a = matrfields.index(datarows[i["hitindex"]][1])
					except:
						if len(matrfields)==0:
							#lalmatr[0][0].append(0)
							matrfields.append(datarows[i["hitindex"]][1])
						else:
							for k in xrange(len(matrfields)):
								lalmatr[k].append(0)
							print lalmatr
							lalmatr.append([])
							matrfields.append(datarows[i["hitindex"]][1])
							for k in xrange(len(matrfields)):
								lalmatr[len(matrfields)-1].append(0)
						#print lalmatr
						#print matrfields
						a = matrfields.index(datarows[i["hitindex"]][1])
							
					try:
						b = matrfields.index(datarows[found["hitindex"]][1])
					except:
						if len(matrfields)==0:
							#lalmatr[0][0].append(0)
							matrfields.append(datarows[found["hitindex"]][1])
						else:
							for k in xrange(len(matrfields)):
								lalmatr[k].append(0)
							lalmatr.append([])
							matrfields.append(datarows[found["hitindex"]][1])
							for k in xrange(len(matrfields)):
								lalmatr[len(matrfields)-1].append(0)
						#print lalmatr
						#print matrfields
						b = matrfields.index(datarows[found["hitindex"]][1])
				
					#lalmatr[  matrfields[datarows[i["hitindex"]][1]]  ][  matrfields[datarows[found["hitindex"]][1]]  ] += 1
					lalmatr[  a  ][  b  ] += 1

					rezstr = outfmt % (	datarows[i["hitindex"]][0], 
										"???", 
										datarows[i["hitindex"]][1], 
										datarows[i["hitindex"]][8], 
										datarows[i["hitindex"]][9], 
										datarows[i["hitindex"]][6], 
										datarows[i["hitindex"]][7],
										datarows[found["hitindex"]][1], 
										datarows[found["hitindex"]][8],
										datarows[found["hitindex"]][9],
										datarows[found["hitindex"]][6],
										datarows[found["hitindex"]][7],
										str(found['query_from'] - i['query_to'])	)
					
					f.write(rezstr) 
					f.flush()

				
				else:
					pass
					#print "No match for " + str(i['query_to'])
				
					
			readctr = readctr + 1
			if (readctr % 100 == 0):
				#print "%9i reads processed ( %6i matches  )" % (readctr, matchctr)
				pass
			
			matchdb = []
		
		# Continue as usual, even after write
			
		#print i_hsp.find('Hsp_num').text
		matchdb.append({	"query_from":	int(datarows[i_row][6]) , \
							"query_to":		int(datarows[i_row][7]) , \
							"hitindex":		i_row, })
		prevprocessed = datarows[i_row][0]

	print "%9i reads processed ( %6i matches  )" % (readctr, matchctr)
	f.close()
		
	print "\nResulting distribution:\n"	
	
	for i in xrange(len(matrfields)):
		print i, matrfields[i]
	print ""
	print "     0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24"
	print "_"*80
	for i in xrange(len(matrfields)):
		sys.stdout.write("%3i|" % i)
		for j in xrange(len(matrfields)):
			sys.stdout.write("%2i " % lalmatr[i][j])
			#print "%4i " % i, lalmatr[i]
		sys.stdout.write("\n")
		
	#export heatmap
	if inputfile!=sys.stdin:
		filename = 'Res_' + os.path.basename(inputfile) + "_heatmap_" + output[:19] + ".txt"
	else:
		filename = 'Res_' + "<stdin>" + "_heatmap_" + output[:19] + ".txt"
	try:
		f = open(filename, 'w')
	except:
		print "Failed to open output heatmap file!"
		sys.exit(2)
	
	for i in xrange(len(matrfields)):
		if i>0:
			f.write("\t")
		f.write(matrfields[i])
	f.write("\n")
	
	for i in xrange(len(matrfields)):
		for j in xrange(len(matrfields)):
			if j>0:
				f.write("\t")
			f.write("%i" % lalmatr[i][j])
		f.write("\n")
	f.close()

if __name__ == "__main__":
   main(sys.argv[1:])

	



